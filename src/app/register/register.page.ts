import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {

    constructor(private activatedRoute: ActivatedRoute,private router: Router,public utilities: Utilities) { }

    public Contrasena:any = "";
    public Contrasena2:any = "";
    public terminosCondiciones:boolean = false;
    public nombres:any = "";
    public apellidos:any = "";

    public user: any = {
        Empresa: 'EMPR2',
        Sucursal: 1,
        Nombre: '',
        DUI: '',
        Correo: '',
        Pass: '',
        TipoRegistro: 'NORMAL',
        Sexo: 'MASCULINO',
        Telefono: '',
        picture: '',
        Estado: 'ALTA'
    };

    ngOnInit() {
        this.nombres = this.activatedRoute.snapshot.paramMap.get('firstName');
        this.apellidos = this.activatedRoute.snapshot.paramMap.get('lastName');
        this.user.Correo = this.activatedRoute.snapshot.paramMap.get('email');
    }

    registrarme = function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.utilities.mostrarAlerta('ALERTA','Por favor, verifica tu conexión a internet.');
        }else{
            if(this.nombres === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese sus nombres');
            }else if(this.apellidos === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese sus apellidos');
            }else if(this.user.Correo === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su correo electrónico');
            }else if(this.user.Telefono === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su número de celular');
            }else if(this.Contrasena === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su contraeña');
            }else if(this.Contrasena2 === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, digite nuevamente su contraseña');
            }else if(this.Contrasena2 != this.Contrasena){
                this.utilities.mostrarAlerta('ALERTA','Por favor, las contraseñas ingresadas no son iguales');
            }else if(this.user.Sexo === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, seleccione su genero');
            }else if(!this.terminosCondiciones){
                this.utilities.mostrarAlerta('ALERTA','Por favor, acepte los terminos y condiciones para continuar');
            }else{
                this.user.Nombre = this.nombres + ' ' + this.apellidos;
                this.user.Pass = this.Contrasena;
                this.utilities.showLoadingController("POR FAVOR ESPERE");
                axios({
                    method: 'post',
                    url: this.utilities.apiURL() + 'api/Crio/InsertCteExpress',
                    timeout: 10000,
                    data: this.user,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    validateStatus: (status) => {
                        return true;
                    }
                }).then(response => {
                    setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
                    if(response.data.result.estado == 400){
                        tis.utilities.mostrarAlerta('ALERTA',response.data.result.mensaje);
                    }else{
                        tis.obtenerInfoCliente(response.data.result.cliente);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
        }
    };

    obtenerInfoCliente = function(codcliente){
		let tis = this;
		axios({
			method: 'post',
			url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
			timeout: 10000,
			data: {
				call: "GetInfoCliente",
				parameters: codcliente
			},
			headers: {
				'Content-Type': 'application/json'
			},
			validateStatus: (status) => {
				return true;
			}
		}).then(response => {
			if (typeof response.data.response.table[0].resultado !== 'undefined') {
				setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 500);
				tis.utilities.mostrarAlerta('ALERTA','No se pudo comprobar que las credenciales ingresadas sean auténticas.');
			}else{
				localStorage.setItem("autenticated", "1");
				localStorage.setItem("codcliente", codcliente);
				localStorage.setItem("nombres", response.data.response.table[0].nombre);
                localStorage.setItem("email", this.user.Correo);
				localStorage.setItem("direccion", response.data.response.table[0].direccion);
				localStorage.setItem("telefonos", response.data.response.table[0].telefonos);
				localStorage.setItem("sexo",response.data.response.table[0].sexo);
				tis.router.navigate(['categories'],{skipLocationChange: true, replaceUrl: true});
				tis.utilities.dismissLoadingController();
			}

		})
		.catch(function (error) {
			console.log(error);
		});
	};

}
