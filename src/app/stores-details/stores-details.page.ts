import { Platform } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';

@Component({
    selector: 'app-detalles-sucursal',
    templateUrl: './stores-details.page.html',
    styleUrls: ['./stores-details.page.scss'],
})
export class StoresDetailsPage implements OnInit {

    @Input()
    urlMap:SafeResourceUrl;
    url:string;

    constructor(private router: Router, private utilities: Utilities, private activatedRoute: ActivatedRoute, public sanitizer:DomSanitizer, private platform: Platform) { }

    public isConnected:boolean = true;
    public isLoaded:boolean = false;

    public store:any = {
        direccion: '',
        imagenSuc: '',
        latitud: '',
        longitud: '',
        nombre: '',
        payback: '',
        telefonos: '',
        descAmplia: ''
    };
    ngOnInit() {
        this.store.direccion = this.activatedRoute.snapshot.paramMap.get('direccion');
        this.store.imagenSuc = this.activatedRoute.snapshot.paramMap.get('imagenSuc');
        this.store.latitud = this.activatedRoute.snapshot.paramMap.get('latitud');
        this.store.longitud = this.activatedRoute.snapshot.paramMap.get('longitud');
        this.store.nombre = this.activatedRoute.snapshot.paramMap.get('nombre');
        this.store.payback = this.activatedRoute.snapshot.paramMap.get('payback');
        this.store.telefonos = this.activatedRoute.snapshot.paramMap.get('telefonos');
        this.store.descAmplia = this.activatedRoute.snapshot.paramMap.get('descAmplia');

        this.url = "https://maps.google.com/maps?width=100%&height=400&hl=es&q=" + this.store.latitud + "," + this.store.longitud + "&ie=UTF8&t=p&z=17&iwloc=B&output=embed";
        this.urlMap = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
        console.log(this.urlMap);
        this.isLoaded = true;
    }

    obtenerDirecciones = function(){
        let location = this.store.latitud + "," + this.store.longitud;;
        if (this.platform.is('android')) {
            window.location.href = 'geo:' + location;
        } else {
            window.location.href = 'maps://maps.apple.com/?ll=' + location;
        }
    }
}
