import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoresDetailsPage } from './stores-details.page';

describe('StoresDetailsPage', () => {
    let component: StoresDetailsPage;
    let fixture: ComponentFixture<StoresDetailsPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ StoresDetailsPage ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StoresDetailsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
