import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Utilities } from '../providers/utilities';
import { Storage } from '@ionic/storage';
import axios from 'axios';

@Component({
    selector: 'app-para-ti',
    templateUrl: './for-you.page.html',
    styleUrls: ['./for-you.page.scss'],
})
export class ForYouPage implements OnInit {

    public productos: any = [];
    public isConnected:boolean = true;
    public user:any = {
        name: '',
        id: ''
    };

    constructor(private router: Router, private storage: Storage, private utilities: Utilities) {
    }

    ngOnInit() {
        this.storage.get('nombres').then((val) => {
            this.user.name = val;
        });
        this.storage.get('codcliente').then((val) => {
            this.user.id = val;
            this.obtenerProductos();
        });
    }

    obtenerProductos = async function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.isConnected = false;
        }else{
            this.isConnected = true;
            this.utilities.showLoadingController("POR FAVOR ESPERE");
            await axios({
                method: 'post',
                url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
                timeout: 10000,
                data: {
                    call: "GetParaMi",
                    parameters: "'" + this.user.id + "'"
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                validateStatus: (status) => {
                    return true;
                }
            }).then(response => {
                tis.productos = response.data.response.table;
                setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 500);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    doRefresh = async function(event) {
        await this.obtenerProductos();
        event.target.complete();
    };

}
