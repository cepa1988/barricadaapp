import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForYouPage } from './for-you.page';

describe('ForYouPage', () => {
  let component: ForYouPage;
  let fixture: ComponentFixture<ForYouPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForYouPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForYouPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
