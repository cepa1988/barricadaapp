import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';

@Component({
    selector: 'app-sucursales',
    templateUrl: './stores.page.html',
    styleUrls: ['./stores.page.scss'],
})
export class StoresPage implements OnInit {

    constructor(private router: Router, private utilities: Utilities) { }

    public isConnected:boolean = true;

    ngOnInit() {
        this.obtenerSucursales();
    }

    public sucursales: any = [];

    obtenerSucursales = async function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.isConnected = false;
        }else{
            this.isConnected = true;
            this.utilities.showLoadingController("POR FAVOR ESPERE");
            await axios({
                method: 'post',
                url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
                timeout: 10000,
                data: {
                    call: "GetSucursalesDesc"
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                validateStatus: (status) => {
                    return true;
                }
            }).then(response => {
                tis.sucursales = response.data.response.table;
                setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    doRefresh = async function(event) {
        await this.obtenerSucursales();
        event.target.complete();
    };

    verTienda = function(data){
        this.router.navigate(['store-details',data]);
    }

}
