import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { Storage } from '@ionic/storage';


const routes: Routes = [

    {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'register',
        loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule)
    },
    {
        path: 'categories',
        loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesPageModule)
    },
    {
        path: 'products',
        loadChildren: () => import('./products/products.module').then(m => m.ProductsPageModule)
    },
    {
        path: 'products-details',
        loadChildren: () => import('./products-details/products-details.module').then(m => m.ProductsDetailsPageModule)
    },
    {
        path: 'promotions',
        loadChildren: () => import('./promotions/promotions.module').then(m => m.PromotionsPageModule)
    },
    {
        path: 'stores',
        loadChildren: () => import('./stores/stores.module').then(m => m.StoresPageModule)
    },
    {
        path: 'store-details',
        loadChildren: () => import('./stores-details/stores-details.module').then(m => m.StoresDetailsPageModule)
    },
    {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
    },
    {
        path: 'edit-profile',
        loadChildren: () => import('./edit-profile/edit-profile.module').then(m => m.EditProfilePageModule)
    },
    {
        path: 'for-you',
        loadChildren: () => import('./for-you/for-you.module').then(m => m.ForYouPageModule)
    },

];

@NgModule({
    imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
