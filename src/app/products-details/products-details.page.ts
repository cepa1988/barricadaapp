import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';

@Component({
    selector: 'app-detalles-producto',
    templateUrl: './products-details.page.html',
    styleUrls: ['./products-details.page.scss'],
})
export class ProductsDetailsPage implements OnInit {

    constructor(private router: Router, private utilities: Utilities, private activatedRoute: ActivatedRoute) { }

    public isConnected:boolean = true;
    public product:any = {
        articulo: '',
        descripcion1: '',
        descripcion2: '',
        categoria: '',
        precioLista: '',
        direccionImg: ''
    };
    ngOnInit() {
        this.product.articulo = this.activatedRoute.snapshot.paramMap.get('articulo');
        this.product.descripcion1 = this.activatedRoute.snapshot.paramMap.get('descripcion1');
        this.product.descripcion2 = this.activatedRoute.snapshot.paramMap.get('descripcion2');
        this.product.categoria = this.activatedRoute.snapshot.paramMap.get('categoria');
        this.product.precioLista = this.activatedRoute.snapshot.paramMap.get('precioLista');
        this.product.direccionImg = this.activatedRoute.snapshot.paramMap.get('direccionImg');
    }
}
