import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsDetailsPage } from './products-details.page';

describe('ProductsDetailsPage', () => {
    let component: ProductsDetailsPage;
    let fixture: ComponentFixture<ProductsDetailsPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ProductsDetailsPage ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductsDetailsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
