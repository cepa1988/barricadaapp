import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import axios from 'axios';

@Injectable()
export class Utilities {

	constructor(private loadingController: LoadingController, private alertController: AlertController, private network: Network) {
	}

	private loading;
	private alert;
	apiURL = function(){
		return "http://69.0.149.70:8083/";
	}

	showLoadingController = async function(message){
		this.loading = await this.loadingController.create({
			message: message,
			spinner: 'dots',
			translucent: true
		});
		return this.loading.present();
	};

	dismissLoadingController = function(){
		this.loading.dismiss();
	};

	verificarConexion = function(){
		let conectado = true;
		if (this.network.type == 'none' || this.network.type == 'unknown' ) {
		 	conectado = false;
		}
		return conectado;

	};

	mostrarAlerta = async function(header,message){
		this.alert = await this.alertController.create({
			header: header,
			message: message,
			buttons: ['Aceptar']
		});
		return this.alert.present();
	}

}
