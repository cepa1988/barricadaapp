import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable()
export class SplitPane {

	public splitPaneState : boolean;
	public activeMenu: boolean;

	constructor(public platform: Platform, public router: Router) {
		this.splitPaneState = false;
		this.activeMenu = true;
	}

	getSplitPane(){
		if(localStorage.getItem('autenticated')){
			if(this.platform.width() > 800){
				this.splitPaneState = true;
			}else{
				this.splitPaneState = false;
			}
		}else{
			this.splitPaneState = false;
		}
		return this.splitPaneState;
	};

	getMenuActive(){
		if(this.router.url === '/profile' || this.router.url === '/products' || this.router.url === '/promotions' || this.router.url === '/stores' || this.router.url === '/for-you' || this.router.url === '/login' || this.router.url === '/register'){
			this.activeMenu = true;
		}else{
			this.activeMenu = false;
		}
		return this.activeMenu;
	};
}
