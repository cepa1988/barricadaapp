import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';

@Component({
    selector: 'app-productos',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

    public categoria:any = "";
    public productos: any = [];
    public filteredProductos: any = [];
    public isConnected:boolean = true;
    public termSearched:any = "";

    constructor(private router: Router, private utilities: Utilities, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.categoria = this.activatedRoute.snapshot.paramMap.get('categoria');
        this.obtenerProductos();
    }

    filterItems = function(){
        return this.productos.filter(item => {
            return item.descripcion1.toLowerCase().indexOf(this.termSearched.toLowerCase()) > -1;
        });
    }


    obtenerProductos = async function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.isConnected = false;
        }else{
            this.isConnected = true;
            this.utilities.showLoadingController("POR FAVOR ESPERE");
            await axios({
                method: 'post',
                url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
                timeout: 10000,
                data: {
                    call: "GetArtFilterImg",
                    parameters: "'" + tis.categoria + "','CATEGORIA','1'"
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                validateStatus: (status) => {
                    return true;
                }
            }).then(response => {
                tis.productos = response.data.response.table;
                this.filterItems();
                setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 500);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    doRefresh = async function(event) {
        await this.obtenerProductos();
        event.target.complete();
    };

    verProducto = function(data){
         this.router.navigate(['products-details',data]);
    };

}
