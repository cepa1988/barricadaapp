import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Utilities } from '../providers/utilities';
import { Storage } from '@ionic/storage';

import axios from 'axios';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	public credenciales: any = {
		email: '',
		password: ''
	};

	public codcliente: any = "";

	public usuario: any = {
		firstName: '',
		lastName: '',
		email: '',
		gender: '',
		picture: ''
	};

	constructor(private router: Router, private fb: Facebook, private storage: Storage, private utilities: Utilities) {
		this.codcliente = "";
	}

	ngOnInit() {
		this.storage.clear();
	}

	iniciarSesion = function(){
		let tis = this;
		if(this.credenciales.email === ''){
			this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su correo electrónico');
		}else if(this.credenciales.password === ''){
			this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su contraseña');
		}else{
			this.utilities.showLoadingController("POR FAVOR ESPERE");
			axios({
				method: 'post',
				url: this.utilities.apiURL() + 'api/Crio/LoginValidate',
				timeout: 10000,
				data: {
					call: "GetLoginValidate",
					parametersValidate: [
						{
							Column: "email",
							Value: this.credenciales.email
						},
						{
							Column: "contraseña",
							Value: this.credenciales.password
						}
					]
				},
				headers: {
					'Content-Type': 'application/json'
				},
				validateStatus: (status) => {
					return true;
				}
			}).then(response => {
				if(response.data.response.table.length == 0){
					setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
					tis.utilities.mostrarAlerta('ALERTA','El correo electrónico ingresado o contraseña no son validos.');
				}else{
					tis.obtenerInfoCliente(response.data.response.table[0].cliente);
				}
			})
			.catch(function (error) {
				console.log(error);
			});
		}
	};

	obtenerInfoCliente = function(codcliente){
		let tis = this;
		axios({
			method: 'post',
			url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
			timeout: 10000,
			data: {
				call: "GetInfoCliente",
				parameters: codcliente
			},
			headers: {
				'Content-Type': 'application/json'
			},
			validateStatus: (status) => {
				return true;
			}
		}).then(response => {
			setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
			if (typeof response.data.response.table[0].resultado !== 'undefined') {
				tis.utilities.mostrarAlerta('ALERTA','No se pudo comprobar que las credenciales ingresadas sean auténticas.');
			}else{
				tis.storage.set('autenticated', '1');
				tis.storage.set('codcliente', codcliente);
				tis.storage.set('nombres', response.data.response.table[0].nombre);
				tis.storage.set('mail', this.credenciales.email);
				tis.storage.set('direccion', response.data.response.table[0].direccion);
				tis.storage.set('telefonos', response.data.response.table[0].telefonos);
				tis.storage.set('sexo', response.data.response.table[0].sexo);
				tis.router.navigate(['categories'],{skipLocationChange: true, replaceUrl: true});
				tis.utilities.dismissLoadingController();
			}

		})
		.catch(function (error) {
			console.log(error);
		});
	};

	loginFB = function(){
		this.fb.login(['public_profile', 'user_friends', 'email'])
		.then((res: FacebookLoginResponse) => this.getInfo())
		.catch(e => console.log('Error logging into Facebook', e));
		this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
	};

	getInfo = function(){
		this.fb.api('/me?fields=first_name,last_name,address,email,gender,picture',['public_profile','email'])
		.then(data=>{
			this.usuario.firstName = data.first_name;
			this.usuario.lastName = data.last_name;
			this.usuario.email = data.email;
			this.usuario.gender = data.gender == "male" ? "Masculino" : "Femenino";
			this.usuario.picture = data.picture.data.url;
			this.verificarEmail();

		})
		.catch(error =>{
			console.error( error );
		});
	};

	verificarEmail = function(){
		let tis = this;
		this.utilities.showLoadingController("POR FAVOR ESPERE");
		axios({
			method: 'post',
			url: this.utilities.apiURL() + 'api/Crio/AccessValidate',
			timeout: 10000,
			data: {
				call: "GetAccessValidate",
				parametersValidate: [
					{
						Column: "email",
						Value: this.usuario.email
					}
				]
			},
			headers: {
				'Content-Type': 'application/json'
			},
			validateStatus: (status) => {
				return true;
			}
		}).then(response => {
			if(response.data.response.table[0].existe == 1){
				tis.codcliente = response.data.response.table[0].cliente;
				tis.obtenerInfoCliente(response.data.response.table[0].cliente);
			}else{
				setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
				this.router.navigate(['register',this.usuario]);
			}
		})
		.catch(function (error) {
			setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
			this.utilities.mostrarAlerta('ALERTA',error);
			return false;
		});
	};

}
