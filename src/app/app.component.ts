import { Component, ViewChildren, QueryList } from '@angular/core';
import { Location } from "@angular/common";
import { Platform, ModalController, ActionSheetController, PopoverController, IonRouterOutlet, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Router } from '@angular/router';
import { SplitPane } from './providers/splitPane';
import { Toast } from '@ionic-native/toast/ngx';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    lastTimeBackPress = 0;
    timePeriodToExit = 2000

    @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

    public appPages = [
        {
            title: 'Categorias',
            url: '/categories',
            icon: 'home',
            class: ''
        },
        {
            title: 'Promociones',
            url: '/promotions',
            icon: 'pricetag',
            class: ''
        },
        {
            title: 'Sucursales',
            url: '/stores',
            icon: 'pin',
            class: ''
        },
        {
            title: 'Para tí',
            url: '/for-you',
            icon: 'star',
            class: ''
        },
        {
            title: 'Mi perfil',
            url: '/profile',
            icon: 'person',
            class: ''
        },
        {
            title: 'Cerrar sesion',
            url: '/login',
            icon: 'hand',
            class: 'last'
        },
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private screenOrientation: ScreenOrientation,
        private statusBar: StatusBar,
        public splitPane: SplitPane,
        public modalCtrl: ModalController,
        private menu: MenuController,
        private actionSheetCtrl: ActionSheetController,
        private popoverCtrl: PopoverController,
        private router: Router,
        private toast: Toast,
        private location: Location,
        private storage: Storage
    ) {
        this.backButtonEvent();
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.backgroundColorByHexString('#363636');
            this.statusBar.styleLightContent();
            this.splashScreen.hide();

            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

            this.storage.get('autenticated').then((val) => {
                if(val > 0){
                    this.router.navigate(['categories'],{skipLocationChange: true, replaceUrl: true});
                }else{
                    this.router.navigate(['login'],{skipLocationChange: true, replaceUrl: true});
                }

            });

        });
    };

    // active hardware back button
    backButtonEvent() {
        this.platform.backButton.subscribe(async () => {
            // close action sheet
            try {
                const element = await this.actionSheetCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            } catch (error) {
            }

            // close popover
            try {
                const element = await this.popoverCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            } catch (error) {
            }

            // close modal
            try {
                const element = await this.modalCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            } catch (error) {
                console.log(error);

            }

            // close side menu
            try {
                const element = await this.menu.getOpen();
                if (element) {
                    this.menu.close();
                    return;

                }

            } catch (error) {
                console.log(error);
            }

            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (outlet && outlet.canGoBack()) {
                    outlet.pop();
                    return;
                }
            });

            if (this.router.url === '/categories' || this.router.url === '/login') {
                if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                    navigator['app'].exitApp(); // work in ionic 4
                    return;
                } else {
                    this.toast.show('Presione de nuevo para salir de la aplicación.','2000','center')
                    .subscribe(toast => {});
                    this.lastTimeBackPress = new Date().getTime();
                    return;
                }
            }else{
                this.router.navigate(['categories'],{skipLocationChange: true, replaceUrl: true});
            }

        });
    }
}
