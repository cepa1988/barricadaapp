import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { Utilities } from '../providers/utilities';
import axios from 'axios';

@Component({
    selector: 'app-categorias',
    templateUrl: './categories.page.html',
    styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

    constructor(private router: Router, private utilities: Utilities) { }

    public isConnected:boolean = true;

    ngOnInit() {
        this.obtenerCategorias();
    }

    public categorias: any = [];

    obtenerCategorias = async function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.isConnected = false;
        }else{
            this.isConnected = true;
            this.utilities.showLoadingController("POR FAVOR ESPERE");
            await axios({
                method: 'post',
                url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
                timeout: 10000,
                data: {
                    call: "GetArtCat"
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                validateStatus: (status) => {
                    return true;
                }
            }).then(response => {
                let data = response.data.response.table;
                for(var k in data) {
                    if(data[k].imagenCat != null){
                        tis.categorias.push(data[k]);
                    }
                }
                // tis.categorias = response.data.response.table;
                setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    };

    doRefresh = async function(event) {
        await this.obtenerCategorias();
        event.target.complete();
    };

    verProductos = function(data){
        this.router.navigate(['products',data]);
    }

}
