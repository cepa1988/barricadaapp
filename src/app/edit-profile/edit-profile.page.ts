import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { Utilities } from '../providers/utilities';
import { Storage } from '@ionic/storage';

import axios from 'axios';
import * as moment from 'moment';

@Component({
    selector: 'app-edit-perfil',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

    public user:any = {
        id: '',
        nombre: '',
        telefonos: '',
        sexo: '',
        email: ''
    };

    public contrasena:any = '';
    public contrasena2:any = '';

    public isConnected:boolean = true;

    constructor(private router: Router, private storage: Storage, private utilities: Utilities) { }

    ngOnInit() {
        this.storage.get('nombres').then((val) => {
            this.user.nombre = val;
        });
        this.storage.get('sexo').then((val) => {
            this.user.sexo = val;
        });
        this.storage.get('telefonos').then((val) => {
            this.user.telefonos = val;
        });
        this.storage.get('mail').then((val) => {
            this.user.email = val;
        });
        this.storage.get('codcliente').then((val) => {
            this.user.id = val;
        });
    }

    editarPerfil = function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.utilities.mostrarAlerta('ALERTA','Por favor, verifica tu conexión a internet.');
        }else{
            if(this.user.nombre === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su nombre');
            }else if(this.user.email === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su correo electrónico');
            }else if(this.user.telefonos === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, ingrese su número de celular');
            }else if(this.user.sexo === ''){
                this.utilities.mostrarAlerta('ALERTA','Por favor, seleccione su genero');
            }else if(tis.contrasena != this.contrasena2){
                this.utilities.mostrarAlerta('ALERTA','Las contraseñas ingresadas no son iguales');
            }else{
                this.utilities.showLoadingController("POR FAVOR ESPERE");
                axios({
                    method: 'post',
                    url: this.utilities.apiURL() + 'api/Crio/SetDataObject',
                    timeout: 10000,
                    data: {
                        call: "UpdateInfoCliente",
                        parameters: tis.user.id,
                        parametersUpdate: [
                            {
                                "Column": "nombre",
                                "Value": tis.user.nombre
                            },
                            {
                                "Column": "email1",
                                "Value": tis.user.email
                            },
                            {
                                "Column": "sexo",
                                "Value": tis.user.sexo
                            },
                            {
                                "Column": "telefonos",
                                "Value": tis.user.telefonos
                            }
                        ]
                    },
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    validateStatus: (status) => {
                        return true;
                    }
                }).then(response => {
                    tis.storage.set('nombres', tis.user.nombre);
                    tis.storage.set('mail', tis.user.email);
                    tis.storage.set('telefonos', tis.user.telefonos);
                    tis.storage.set('sexo', tis.user.sexo);
                    if(tis.contrasena !== ''){
                        tis.actualizarPassword();
                    }else{
                        setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
                        tis.utilities.mostrarAlerta('ALERTA','DATOS DE USUARIO ACTUALIZADOS EXITOSAMENTE');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
        }
    };

    actualizarPassword = function(){
        let tis = this;
        axios({
            method: 'post',
            url: this.utilities.apiURL() + 'api/Crio/SetDataObject',
            timeout: 10000,
            data: {
                call: "UpdateInfoCliente",
                parameters: tis.user.id,
                parametersUpdate: [
                    {
                        "Column": "contrasena",
                        "Value": tis.contrasena
                    }
                ]
            },
            headers: {
                'Content-Type': 'application/json'
            },
            validateStatus: (status) => {
                return true;
            }
        }).then(response => {
            tis.contrasena = '';
            tis.contrasena2 = '';
            setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
            tis.utilities.mostrarAlerta('ALERTA','DATOS DE USUARIO ACTUALIZADOS EXITOSAMENTE');
        })
        .catch(function (error) {
            console.log(error);
        });
    }

}
