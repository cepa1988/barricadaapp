import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { Utilities } from '../providers/utilities';
import { Storage } from '@ionic/storage';

import axios from 'axios';
import * as moment from 'moment';

@Component({
    selector: 'app-mi-perfil',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    public user:any = {
        name: '',
        id: ''
    };
    public bills:any = [];
    public isConnected:boolean = true;

    constructor(private router: Router, private storage: Storage, private utilities: Utilities) { }

    ngOnInit() {
        this.storage.get('nombres').then((val) => {
            this.user.name = val;
        });
        this.storage.get('codcliente').then((val) => {
            this.user.id = val;
            this.obtenerFacturas();
        });
    }

    ionViewWillEnter() {
        this.storage.get('nombres').then((val) => {
            this.user.name = val;
        });
    }

    obtenerFacturas = function(){
        let tis = this;
        if(!this.utilities.verificarConexion()){
            this.isConnected = false;
        }else{
            this.isConnected = true;
            this.utilities.showLoadingController("POR FAVOR ESPERE");
            axios({
                method: 'post',
                url: this.utilities.apiURL() + 'api/Crio/GetDataObject',
                timeout: 10000,
                data: {
                    call: "GetDetailByClient",
                    parameters: "'" + this.user.id + "'"
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                validateStatus: (status) => {
                    return true;
                }
            }).then(response => {
                tis.bills = response.data.response.table;
                setTimeout(function(){ tis.utilities.dismissLoadingController(); }, 1000);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    };

    editarPerfil = function(){
        this.router.navigate(['edit-profile']);
    }

    formatearFecha = function(date){
        return moment(date).format("DD/MM/YYYY");
    }

}
